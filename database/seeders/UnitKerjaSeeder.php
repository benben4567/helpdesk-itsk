<?php

namespace Database\Seeders;

use App\Models\UnitKerja;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UnitKerjaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        UnitKerja::create([
            'unit_kerja_karyawan' => 'TU',
        ]);
        UnitKerja::create([
            'unit_kerja_karyawan' => 'OB',
        ]);
        UnitKerja::create([
            'unit_kerja_karyawan' => 'BEM',
        ]);
    }
}
