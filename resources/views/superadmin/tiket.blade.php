@extends('superadmin.template.main')

@section('title', 'Data Tiket - Helpdesk ITSK')

@section('content')
    <div class="page-content mt-n4">
        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-center mb-4" id="top-content">
                            <h6 class="card-title m-0">Data Tiket Pengaduan</h6>
                            <button type="button" onclick="hapusData()" class="btn btn-danger btn-sm btn-icon-text"
                                id="bt-del"><i class="link-icon" data-feather="x-square"></i> Hapus Data</button>
                        </div>
                        <div class="table-responsive">
                            <table id="tabelTiket" class="table hover stripe" style="width:100%">
                                <thead>
                                    <tr>
                                        <th><input type="checkbox" name="" data-id="' + row.id +'"
                                                class="form-check-input check-all"></th>
                                        <th>Permasalahan</th>
                                        <th>Tanggal Dibuat</th>
                                        <th>Kategori Masalah</th>
                                        <th>Status</th>
                                        <th>Urgensi</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                {{-- <tbody>
                                    @foreach ($tikets as $data)
                                        <tr>
                                            <td><input type="checkbox" class="form-check-input check"></td>
                                            <td>{{ $data->judul_tiket }}</td>
                                            <td>{{ $data->waktu_tiket }}</td>
                                            <td>{{ $data->kategori_laporan }}</td>
                                            <td>{!! $data->status_tiket !!}</td>
                                            <td>{!! $data->tingkat_urgensi !!}</td>
                                            <td><button type="button" id="bt-detail"
                                                    class="btn btn-secondary btn-sm btn-icon-text"><i class="link-icon"
                                                        data-feather="eye" data-bs-toggle="modal"
                                                        data-bs-target="#modalDetail"></i> </button>
                                                <button type="button" id="bt-edit"
                                                    class="btn btn-success btn-sm btn-icon-text"><i class="link-icon"
                                                        data-feather="edit" data-bs-toggle="modal"
                                                        data-bs-target="#modalEdit"></i> </button>
                                            </td>
                                        </tr>
                                    @endforeach --}}
                                {{-- </tbody> --}}
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modals')
    <div id="modalEdit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalEditLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalEditLabel">Edit Tiket
                    </h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form id="formEdit" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="mb-3">
                            <label for="permasalahan" class="form-label">Permasalahan</label>
                            <input type="text" class="form-control" id="permasalahanEdit" name="judul_tiket"
                                placeholder="Masukkan Permasalahan">
                        </div>
                        <div class="mb-3">
                            <label for="tanggalDibuat" class="form-label">Tanggal Dibuat</label>
                            <input type="date" class="form-control" id="tanggalEdit" name="waktu_tiket">
                        </div>
                        <div class="mb-3">
                            <label for="kategori" class="form-label">Kategori Masalah</label>
                            <select class="form-control" id="kategori" name="kategori_laporan">
                                <option selected disabled>Pilih Kategori Masalah</option>
                                <option value="Teknis">Teknis</option>
                                <option value="Akademis">Akademis</option>
                                <option value="Administratif">Administratif</option>
                                <option value="Lainnya">Lainnya</option>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="status" class="form-label">Status Tiket</label>
                            <select class="form-control" id="status" name="status_tiket">
                                <option selected disabled>Pilih Status Tiket</option>
                                <option value="Belum Diproses">Belum Diproses</option>
                                <option value="Sedang Diproses">Sedang Diproses</option>
                                <option value="Selesai">Selesai</option>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="urgensi" class="form-label">Urgensi Tiket</label>
                            <select class="form-control" id="urgensi" name="tingkat_urgensi">
                                <option selected disabled>Pilih Urgensi Tiket</option>
                                <option value="Rendah">Rendah</option>
                                <option value="Sedang">Sedang</option>
                                <option value="Tinggi">Tinggi</option>
                            </select>
                        </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="id" id="id">
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success"><i class="fas fa-save"></i> Simpan</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <div id="modalDetail" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalDetailLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalDetailLabel">Detail Tiket</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="mb-3">
                            <label for="detailJudulTiket" class="form-label">Permasalahan: </label>
                            <textarea class="form-control" id="detailJudulTiket" rows="3" readonly></textarea>
                        </div>
                        <div class="mb-3">
                            <label for="detailWaktuTiket" class="form-label">Tanggal Dibuat:</label>
                            <input type="text" class="form-control" id="detailWaktuTiket" readonly>
                        </div>
                        <div class="mb-3">
                            <label for="detailKategoriLaporan" class="form-label">Kategori Masalah:</label>
                            <input type="text" class="form-control" id="detailKategoriLaporan" readonly>
                        </div>
                        <div class="mb-3">
                            <label for="detailStatusTiket" class="form-label">Status:</label>
                            <input type="text" class="form-control" id="detailStatusTiket" readonly>
                        </div>
                        <div class="mb-3">
                            <label for="detailTiketUrgensi" class="form-label">Urgensi:</label>
                            <input type="email" class="form-control" id="detailTiketUrgensi" readonly>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        const checkAllCheckbox = document.querySelector('.check-all');
        const checkboxes = document.querySelectorAll('.check');

        checkAllCheckbox.addEventListener('change', function() {
            checkboxes.forEach(function(checkbox) {
                checkbox.checked = checkAllCheckbox.checked;
            });
        });

        $(document).ready(function() {
            $('#modalEdit').on('hidden.bs.modal', function() {
                $(this).find('input[type=text], input[type=date]').val('');
                $(this).find('select').prop('selectedIndex', 0);
            });
        });


        var tabel;
        // read data pengguna
        $(document).ready(function() {
            tabel = $('#tabelTiket').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('datatiket') }}",
                columns: [{
                        data: 'id',
                        name: 'id',
                        render: function(data, type, row, meta) {
                            return '<input type="checkbox" name="dataHapus[]" class="form-check-input check ceklis" value="' +
                                data + '" data-id="' + data + '">';
                        },
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'judul_tiket',
                        name: 'judul_tiket'
                    },
                    {
                        data: 'waktu_tiket',
                        name: 'waktu_tiket',
                        // render: function(data,type,row,meta) {
                        //     // Ubah format waktu dari "2024-04-25 10:31:21" menjadi "tahun-bulan-tanggal"
                        //     var date = new Date(data);
                        //     var formattedDate = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);

                        //     return formattedDate;
                        // }
                    },
                    {
                        data: 'kategori_laporan',
                        name: 'kategori_laporan',

                    },
                    {
                        data: 'status_tiket',
                        name: 'status_tiket',
                        render: function(data, type, row, meta) {
                            var badgeClass = data === 'Selesai' ? 'bg-success' : (data ===
                                'Sedang Diproses' ? 'bg-warning' : 'bg-danger');
                            return '<span class="badge ' + badgeClass + '">' + data + '</span>';
                        }
                    },
                    {
                        data: 'tingkat_urgensi',
                        name: 'tingkat_urgensi',
                        render: function(data, type, row, meta) {
                            var badgeClass = data === 'Rendah' ? 'bg-success' : (data ===
                                'Sedang' ? 'bg-warning' : 'bg-danger');
                            return '<span class="badge ' + badgeClass + '">' + data + '</span>';
                        }
                    },
                    {
                        data: null,
                        name: 'aksi',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, row, meta) {
                            return `<button type="button" onclick="modalDetail('${row.judul_tiket}','${row.waktu_tiket}','${row.kategori_laporan}','${row.status_tiket}','${row.tingkat_urgensi}')"class="btn btn-secondary btn-sm btn-icon-text"><i 
                                                class="link-icon" data-feather="eye" data-bs-toggle="modal"
                                                    data-bs-target="#modalDetail"></i> </button>
                                    <button type="button" onclick="modalEdit('${row.id}','${row.judul_tiket}','${row.waktu_tiket}','${row.kategori_laporan}','${row.status_tiket}','${row.tingkat_urgensi}')" class="btn btn-success btn-sm btn-icon-text"><i
                                                class="link-icon" data-feather="edit" data-bs-toggle="modal"
                                                    data-bs-target="#modalEdit" onclick="#"></i> </button>`;
                        }
                    }
                ],
                aLengthMenu: [
                    [10, 30, 50, -1],
                    [10, 30, 50, "All"]
                ],
                iDisplayLength: 10,
                language: {
                    search: "",
                    paginate: {
                        previous: "Sebelumnya",
                        next: "Selanjutnya"
                    },
                    info: "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                    search: "Cari:",
                    lengthMenu: "Tampilkan _MENU_ entri",
                    zeroRecords: "Tidak ditemukan data yang sesuai",
                    infoEmpty: "Menampilkan 0 sampai 0 dari 0 entri",
                    infoFiltered: "(disaring dari _MAX_ entri keseluruhan)"
                },
                responsive: true,
                drawCallback: function(settings) {
                    feather.replace();

                    // $('#formEdit').on('submit', function(e) {
                    //     e.preventDefault();
                    //     let data = $(this).serialize();

                    //     $.ajax({
                    //         url: "{{ route('tiket.update') }}",
                    //         type: "POST",
                    //         data: data,
                    //         success: function(response) {
                    //             console.log(response);
                    //             Swal.fire({
                    //                 title: 'Berhasil',
                    //                 text: 'Data berhasil diubah',
                    //                 icon: 'success',
                    //                 confirmButtonText: 'OK'
                    //             });
                    //             $('#modalEdit').modal('hide');
                    //             tabel.ajax.reload();
                    //         },
                    //         error: function(xhr) {
                    //             console.log(xhr.responseJSON.message);
                    //             Swal.fire({
                    //                 title: 'Gagal',
                    //                 text: xhr.responseJSON.message,
                    //                 icon: 'error',
                    //                 confirmButtonText: 'OK'
                    //             });

                    //         }
                    //     });
                    // });

                    // function modalEdit(id, judul_tiket, waktu_tiket, kategori_laporan, status_tiket,
                    //     tingkat_urgensi) {
                    //     $('#id').val(id);
                    //     $('#permasalahanEdit').val(judul_tiket);
                    //     $('#tanggalEdit').val(waktu_tiket);
                    //     $('#kategori').val(kategori_laporan);
                    //     $('#status').val(status_tiket);
                    //     $('#urgensi').val(tingkat_urgensi);
                    //     $('#modalEdit').modal('show');
                    // }

                    // function modalDetail(judul_tiket, waktu_tiket, kategori_laporan, status_tiket,
                    //     tiket_urgensi) {
                    //     $('#detailJudulTiket').val(judul_tiket);
                    //     $('#detailWaktuTiket').val(waktu_tiket);
                    //     $('#detailKategoriLaporan').val(kategori_laporan);
                    //     $('#detailStatusTiket').val(status_tiket);
                    //     $('#detailTiketUrgensi').val(tiket_urgensi);
                    //     $('#modalDetail').modal('show');
                    // }


                    // $(document).on('click', '.detail-btn', function() {
                    //     var judul_tiket = $(this).data('judul_tiket');
                    //     var waktu_tiket = $(this).data('waktu_tiket');
                    //     var kategori_laporan = $(this).data('kategori_laporan');
                    //     var status_tiket = $(this).data('status_tiket');
                    //     var tiket_urgensi = $(this).data('tiket_urgensi');

                    //     modalDetail(judul_tiket, waktu_tiket, kategori_laporan, status_tiket,
                    //         tiket_urgensi);
                    // });
                },
                initComplete: function() {
                    feather.replace(); // Pastikan feather icons di-load setelah DataTables siap
                }
            });

            $('#tabelTiket').each(function() {
                var datatable = $(this);
                var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
                search_input.attr('placeholder', 'Cari');
                search_input.removeClass('form-control-sm');
                var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
                length_sel.removeClass('form-control-sm');
            });

            $('.check-all').on('change', function() {
                var isChecked = $(this).is(':checked');
                $('.check').prop('checked', isChecked);
            });

            tabel.on('responsive-display.dt', function(e, datatable, row, showHide, update) {
                feather.replace();
            });
        });

        // hapus data pengguna
        function hapusData() {
            var dataHapus = [];
            $('.ceklis:checked').each(function() {
                dataHapus.push($(this).val());
            });

            if (dataHapus.length > 0) {
                Swal.fire({
                    title: 'Anda yakin?',
                    text: 'Data yang dihapus tidak dapat dikembalikan!',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#6c757d',
                    confirmButtonText: 'Ya, hapus!',
                    cancelButtonText: 'Batal'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            url: "{{ route('tiket.delete') }}",
                            type: "DELETE",
                            data: {
                                dataHapus: dataHapus,
                                _token: "{{ csrf_token() }}",
                                _method: "DELETE"
                            },
                            success: function(response) {
                                console.log(response);
                                Swal.fire({
                                    title: 'Berhasil',
                                    text: 'Data berhasil dihapus',
                                    icon: 'success',
                                    confirmButtonText: 'OK'
                                });
                                tabel.ajax.reload();
                            },
                            error: function(xhr) {
                                console.log(xhr.responseJSON.message);
                                Swal.fire({
                                    title: 'Gagal',
                                    text: xhr.responseJSON.message,
                                    icon: 'error',
                                    confirmButtonText: 'OK'
                                });
                            }
                        });
                    }
                });
            } else {
                Swal.fire({
                    title: 'Peringatan',
                    text: 'Pilih data yang akan dihapus',
                    icon: 'warning',
                    confirmButtonText: 'OK'
                });
            }
        }


        $('#formEdit').on('submit', function(e) {
            e.preventDefault();
            let data = $(this).serialize();

            $.ajax({
                url: "{{ route('tiket.update') }}",
                type: "POST",
                data: data,
                success: function(response) {
                    console.log(response);
                    Swal.fire({
                        title: 'Berhasil',
                        text: 'Data berhasil diubah',
                        icon: 'success',
                        confirmButtonText: 'OK'
                    });
                    $('#modalEdit').modal('hide');
                    tabel.ajax.reload();
                },
                error: function(xhr) {
                    console.log(xhr.responseJSON.message);
                    Swal.fire({
                        title: 'Gagal',
                        text: xhr.responseJSON.message,
                        icon: 'error',
                        confirmButtonText: 'OK'
                    });

                }
            });
        });

        function modalEdit(id, judul_tiket, waktu_tiket, kategori_laporan, status_tiket, tingkat_urgensi) {
            $('#id').val(id);
            $('#permasalahanEdit').val(judul_tiket);
            $('#tanggalEdit').val(waktu_tiket);
            $('#kategori').val(kategori_laporan);
            $('#status').val(status_tiket);
            $('#urgensi').val(tingkat_urgensi);
            $('#modalEdit').modal('show');
        }


        $('#tabelTiket').on('click', '.btn-edit', function() {
            var id = $(this).data('id');
            console.log('sds');
            // alert('id = ' + id);
            // Lakukan logika edit data sesuai dengan ID yang dipilih
            // Contoh: Redirect ke halaman edit dengan ID
            // window.location.href = "{{ url('superadmin/pengguna/edit') }}" + '/' + id;
        });

        function modalDetail(judul_tiket, waktu_tiket, kategori_laporan, status_tiket, tiket_urgensi) {
            $('#detailJudulTiket').val(judul_tiket);
            $('#detailWaktuTiket').val(waktu_tiket);
            $('#detailKategoriLaporan').val(kategori_laporan);
            $('#detailStatusTiket').val(status_tiket);
            $('#detailTiketUrgensi').val(tiket_urgensi);
            $('#modalDetail').modal('show');
        }

        $(document).on('click', '.detail-btn', function() {
            var judul_tiket = $(this).data('judul_tiket');
            var waktu_tiket = $(this).data('waktu_tiket');
            var kategori_laporan = $(this).data('kategori_laporan');
            var status_tiket = $(this).data('status_tiket');
            var tiket_urgensi = $(this).data('tiket_urgensi');

            modalDetail(judul_tiket, waktu_tiket, kategori_laporan, status_tiket, tiket_urgensi);
        });

        $(window).resize(function() {
            $('#tabelTiket').DataTable().columns.adjust().responsive.recalc();
        });
    </script>
@endpush

@push('style')
    <style>
        .btn-secondary {
            margin-right: 5px;
        }

        .link-icon {
            max-width: 20px;
        }

        .page-item.active .page-link {
            background-color: #14A44D !important;
            border-color: #14A44D !important;
            color: white !important;
        }

        .page-link {
            color: #333333 !important;
        }

        #tabelTiket thead th:first-child {
            cursor: default;
        }

        #tabelTiket thead th:first-child::after,
        #tabelTiket thead th:first-child::before {
            display: none !important;
            pointer-events: none;
        }

        .dataTables_empty {
            text-align: center !important;
        }

        #tabelTiket td,
        #tabelTiket th {
            text-align: center;
        }

        #tabelTiket td.child {
            text-align: left;
        }

        @media only screen and (max-width: 768px) {
            #tabelTiket td {
                white-space: normal;
                word-wrap: break-word;
            }

            #tabelTiket_filter {
                margin-top: 10px;
            }
        }

        @media only screen and (max-width: 476px) {
            #top-content {
                flex-direction: column;
            }

            #bt-del {
                margin-top: 10px;
                width: 72%;
            }

        }
    </style>
@endpush
