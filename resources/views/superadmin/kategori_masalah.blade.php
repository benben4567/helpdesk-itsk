@extends('superadmin.template.main')

@section('title', 'Data Kategori Masalah - Helpdesk ITSK')

@section('content')
    <div class="page-content mt-n4">
        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-center mb-4" id="top-content">
                            <h6 class="card-title m-0">Data Kategori Masalah</h6>
                        </div>
                        <div class="table-responsive">
                            <table id="TabelKategoriMasalah" class="table hover stripe" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Kategori</th>
                                        <th>Unit Kerja</th>
                                        <th>Jumlah</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $iteration = 1; ?>
                                    @foreach ($jumlahKategori as $kategori => $unitKerja)
                                        @foreach ($unitKerja as $unitKerjaKaryawan => $jumlah)
                                            <tr>
                                                <td>{{ $iteration++ }}</td>
                                                <td>{{ $kategori }}</td>
                                                <td>{{ $unitKerjaKaryawan }}</td>
                                                <td>{{ $jumlah }}</td>
                                            </tr>
                                        @endforeach
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('script')
    <script>
        // const checkAllCheckbox = document.querySelector('.check-all');
        // const checkboxes = document.querySelectorAll('.check');

        // checkAllCheckbox.addEventListener('change', function() {
        //     checkboxes.forEach(function(checkbox) {
        //         checkbox.checked = checkAllCheckbox.checked;
        //     });
        // });

        // $(document).ready(function() {
        //     $('#modalTambah, #modalEdit').on('hidden.bs.modal', function() {
        //         $(this).find('input[type=text], input[type=number]').val('');
        //         $(this).find('select').val('Pilih Unit Kerja');
        //     });
        // });

        //     document.addEventListener('DOMContentLoaded', function () {
        //     // Ambil elemen tombol detail dari setiap baris tabel
        //     const detailButtons = document.querySelectorAll('#TabelKategoriMasalah #bt-detail');

        //     // Tambahkan event listener untuk setiap tombol detail
        //     detailButtons.forEach(function (button, index) {
        //         button.addEventListener('click', function () {
        //             // Ambil nilai data dari baris tabel terkait
        //             const namaKategori = document.querySelectorAll('#TabelKategoriMasalah tbody tr')[index].querySelector('td:nth-child(2)').textContent;
        //             const unitKerja = document.querySelectorAll('#TabelKategoriMasalah tbody tr')[index].querySelector('td:nth-child(3)').textContent;
        //             const jumlahAnggota = document.querySelectorAll('#TabelKategoriMasalah tbody tr')[index].querySelector('td:nth-child(4)').textContent;

        //             // Tampilkan nilai data ke dalam modaledit
        //             document.getElementById('detailNamaKategori').textContent = namaKategori;
        //             document.getElementById('detailUnitKerja').textContent = unitKerja;
        //             document.getElementById('detailJumlahAnggota').textContent = jumlahAnggota;
        //         });
        //     });
        // });

        $(function() {
            $('#TabelKategoriMasalah').DataTable({
                "aLengthMenu": [
                    [10, 30, 50, -1],
                    [10, 30, 50, "All"]
                ],
                "iDisplayLength": 10,
                "language": {
                    search: "",
                    "paginate": {
                        "previous": "Sebelumnya",
                        "next": "Selanjutnya"
                    },
                    "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                    "search": "Cari:",
                    "lengthMenu": "Tampilkan _MENU_ entri",
                    "zeroRecords": "Tidak ditemukan data yang sesuai",
                    "infoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
                    "infoFiltered": "(disaring dari _MAX_ entri keseluruhan)"
                },
                "responsive": true
            });

            $('#TabelKategoriMasalah').each(function() {
                var datatable = $(this);
                var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
                search_input.attr('placeholder', 'Cari');
                search_input.removeClass('form-control-sm');
                var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
                length_sel.removeClass('form-control-sm');
            });
        });

        $(window).resize(function() {
            $('#TabelKategoriMasalah').DataTable().columns.adjust().responsive.recalc();
        });
    </script>
@endpush

@push('style')
    <style>
        .link-icon {
            max-width: 20px;
        }

        .page-item.active .page-link {
            background-color: #14A44D !important;
            border-color: #14A44D !important;
            color: white !important;
        }

        .page-link {
            color: #333333 !important;
        }

        .dataTables_empty {
            text-align: center !important;
        }

        #TabelKategoriMasalah td,
        #TabelKategoriMasalah th {
            text-align: center;
        }

        #TabelKategoriMasalah td.child{
            text-align: left;
        }


        @media only screen and (max-width: 768px) {
            #TabelKategoriMasalah_filter {
                margin-top: 10px;
            }

            #TabelKategoriMasalah td {
                white-space: normal;
                word-wrap: break-word;
            }
        }

        @media only screen and (max-width: 556px) {
            #top-content {
                flex-direction: column;
            }

            .bt-group {
                flex-direction: column;
            }

            #bt-tambah {
                width: 100%;
                margin-top: 10px;
            }

            #bt-del {
                width: 100%;
                margin-top: 10px;
            }
        }
    </style>
@endpush
