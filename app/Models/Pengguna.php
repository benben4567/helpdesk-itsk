<?php

namespace App\Models;

use App\Models\Profil;
use App\Models\RiwayatTiket;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Pengguna extends Authenticatable
{
    use HasFactory,Notifiable,CanResetPassword,SoftDeletes;

    protected $table = 'pengguna';
    protected $rememberTokenName = '';
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'email',
        'password',
        'role',
        'status',
    ];

    public $timestamps = false;

    public function tiket()
    {
        return $this->hasMany(Tiket::class, 'id_pengguna', 'id');
    }

    public function profil()
    {
        return $this->hasOne(Profil::class,  'id_pengguna', 'id');
    }

    public function riwayat() {
        return $this->hasMany(RiwayatTiket::class, 'id_pengguna', 'id');
    }
}
