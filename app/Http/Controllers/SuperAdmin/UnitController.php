<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Models\Pengguna;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UnitController extends Controller
{
    public function jumlahunit(){
        // Ambil semua nilai unik dari kolom 'role' pada tabel pengguna
    $roles = Pengguna::select('role', DB::raw('COUNT(*) as jumlah'))
    ->groupBy('role')
    ->get();

    return view('superadmin.unit', ['dataJumlahRole' => $roles]);
    }
}
